#!/bin/bash

# Open a debug port
JAVA_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n"

# Set memory options
JAVA_OPTS="$JAVA_OPTS -Xms256m -Xmx1024m -XX:MaxPermSize=256m"

# Felix property file
JAVA_OPTS="$JAVA_OPTS -Dfelix.config.properties=file:conf/amdatu-platform.properties"

# Set encoding to UTF-8
JAVA_OPTS="$JAVA_OPTS -Dfile.encoding=utf-8"

# Run the platform...
java $JAVA_OPTS -jar lib/org.apache.felix.main-${org.apache.felix.main.version}.jar
