Amdatu MgmtServer Readme 
========================

This Amdatu Management Server project maintains the tools and application 
used to manage a cluster of Amdatu nodes. It heavily relies on the Apache 
ACE that implements a flexible provisioning mechanism based on the OSGi 
DeploymentAdmin standard.

 * WIKI -> http://www.amdatu.org/confluence/display/Amdatu/Amdatu+Management+Server
 * JIRA -> http://jira.amdatu.org/jira/browse/AMDATUMNGMNT


Repository
==========

The Amdatu Management Server repository uses Git and is hosted on Bitbucket. 
You can find it here: https://bitbucket.org/amdatu/amdatu-mgmtserver

Building
========

The Amdatu build infrastructure is based on Maven 2. The default goal builds,
tests, packages and installs all artifacts.